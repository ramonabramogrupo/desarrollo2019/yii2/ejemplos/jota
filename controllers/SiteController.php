<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use app\models\Contacto;
use app\models\Productos;
use yii\data\ActiveDataProvider;
use app\models\Categorias;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index',[
            "titulo"=>"Productos de la JOTA",
            "foto"=>"jota.jpg",
            "texto"=>"ejemplo de clase con una web"
        ]);
    }
    
    public function actionProductos(){
        //$query="select * from productos";
        $activeQuery=Productos::find();
        $dataProvider=new ActiveDataProvider([
            "query"=>$activeQuery,
        ]);
        
        return $this->render("productos",[
            "data"=>$dataProvider,
        ]);
        
    }
    
     public function actionOfertas(){
        //$query="select * from productos where oferta=1";
        $activeQuery=Productos::find()
                ->where(["oferta"=>1]);
        $dataProvider=new ActiveDataProvider([
            "query"=>$activeQuery,
        ]);
        
        return $this->render("productos",[
            "data"=>$dataProvider,
        ]);
    }

    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    public function actionInformacion(){
        $model=new ContactForm();
        if($model->load(Yii::$app->request->post())
                && 
            $model->contact(Yii::$app->params["informacion"])){
            Yii::$app->session->setFlash('enviadaInformacion');
            return $this->refresh();
        }
        
        return $this->render("informacion",[
            "model"=>$model,
        ]);
                
    }
    
     public function actionContacto(){
        $model=new Contacto();
        if($model->load(Yii::$app->request->post())
                && 
            $model->contact()){
            Yii::$app->session->setFlash('enviadaInformacion');
            return $this->refresh();
        }
        
        return $this->render("contacto",[
            "model"=>$model,
        ]);
                
    }
    
    
    /**
     * prueba de envio de correo electronico desde informacion
     */
    public function actionCorreo(){
        $correo=new ContactForm();
        $correo->asunto="Probando este rollo";
        $correo->contenido="El contenido del correo";
        $correo->correo="cliente@correo.es";
        $correo->nombre="Cliente";
        $correo->contact(Yii::$app->params["informacion"]);
    }
    
    
        /**
     * prueba de envio de correo electronico desde contacto
     */
    public function actionContactoPrueba(){
        $correo=new Contacto();
        $correo->asunto="Probando este rollo";
        $correo->temas=0;
        $correo->correo="cliente@correo.es";
        $correo->nombre="Cliente";
        $correo->apellidos="Cliente";
        $correo->contact();
    }
    
    public function actionCategorias(){
        
        $query= Categorias::find();
        
        $dataProvider=new ActiveDataProvider([
            "query"=>$query,
            "pagination"=>[
                "pageSize"=>2,
            ],
        ]);
        
        return $this->render("categorias",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionMostrarcategoria($id) {
        $categoria= Categorias::findOne($id);
        //$categoria= Categorias::find()->where(["id"=>$id])->one();
        
        $consultaProductos= Productos::find()
                ->joinWith("relacions",FALSE,"INNER JOIN")
                ->where(["categoria"=>$id]);
        $dataProvider=new ActiveDataProvider([
            "query"=>$consultaProductos,
        ]);
        return $this->render("mostrarCategoria",[
            "modelo"=>$categoria,
            "dataProvider"=>$dataProvider,
        ]);
        
        
    }
    
}
