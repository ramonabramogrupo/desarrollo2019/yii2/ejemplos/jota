﻿DROP DATABASE IF EXISTS jota;
CREATE DATABASE jota;
USE jota;
CREATE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(20),
  foto varchar(20),
  descripcion varchar(255),
  precio float,
  oferta boolean,
  PRIMARY KEY (id)
  );

INSERT INTO productos (nombre, foto, descripcion, precio, oferta)
  VALUES ('lapices', 'lapiz.jpg', 'un lapiz', 1, FALSE);