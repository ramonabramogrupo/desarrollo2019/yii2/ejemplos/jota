<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Description of Miniaturas
 *
 * @author ramon
 */
class Listado extends Widget {

    public $valores;
    public $campo;

    public function init() {
        parent::init();
    }

    public function run() {
        $salida="<ul>";
        foreach($this->valores as $c){
            $salida.="<li>" . $c[$this->campo] . "</li>";
        }
        $salida.="</ul>";
        return $salida;
    }

}
