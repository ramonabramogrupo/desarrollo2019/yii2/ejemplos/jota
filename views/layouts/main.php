<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;




AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'encodeLabels'=>false,
        'items' => [
            ['label' => '<i class="glyphicon glyphicon-home"></i> Inicio', 'url' => ['/site/index']],
            ['label' => '<i class="fa fa-check" aria-hidden="true"> </i> Ofertas', 'url' => ['/site/ofertas']],
            ['label' => '<i class="fa fa-cube" aria-hidden="true"> </i> Productos', 'url' => ['/site/productos']],
            ['label' => '<i class="fa fa-star" aria-hidden="true"> </i> Categorias', 'url' => ['/site/categorias']],
            ['label'=>'<i class="fa fa-comment-o" aria-hidden="true"> </i> Nosotros', 'items'=>[
                ['label' => 'Donde estamos', 'url' => ['/site/donde']],
                ['label' => 'Quienes somos', 'url' => ['/site/donde']],
                ['label' => 'Nuestros productos', 'url' => ['/site/donde']],
               '<li class="divider">&nbsp</li>',
                ['label' => 'Informacion', 'url' => ['/site/informacion']],
            ]],
            ['label' => '<i class="fa fa-address-book" aria-hidden="true"> </i> Contacto', 'url' => ['/site/contacto']],
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
